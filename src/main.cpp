#include <Arduino.h>
#include <SPI.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <Ticker.h>
#include "../include/consts.h"
#include "routing/DefaultController.h"
#include "models/LedRgb.h"
#include "drivers/LedRgbDriver.h"

LedRgb* ledRgb;
ESP8266WebServer* httpServer;
DefaultController* defaultController;

// void printMessage()
// {
//     Serial.println("Test123");
// }

// Ticker timer1(printMessage, 4000, MILLIS);

void log(String message)
{
    Serial.println(message);
}

void setup() {
    Serial.begin(9600);

    analogWriteRange(255); // for 8 bit PWM adj

    ledRgb = new LedRgb(PIN_RED, PIN_GREEN, PIN_BLUE);
    httpServer = new ESP8266WebServer(HTTP_SERVER_PORT);
    defaultController = new DefaultController(httpServer, ledRgb);

    log("LED RGB Driver");

    WiFi.mode(WIFI_STA);
    WiFi.begin(STASSID, STAPSK);

    log("Wait for WiFi connection");

    while (WiFi.status() != WL_CONNECTED) {
        yield(); // this must be, becouse in other way watchdog reset all the system
    };

    log("WiFi connected!");

    if (MDNS.begin("esp8266")) {
        log("MDNS responder started");
    }

    httpServer->on("/", []() {
        defaultController->mainRoute(); 
    });
    httpServer->on("/api/v1/color", HTTP_GET, []() {
        defaultController->getColorRoute();
    });
    httpServer->on("/api/v1/color", HTTP_POST, []() {
        defaultController->changeColorRoute();
    });
    httpServer->on("/api/v1/status", HTTP_GET, []() {
        defaultController->statusGetRoute();
    });
    httpServer->on("/api/v1/status", HTTP_POST, []() {
        defaultController->changeStatusRoute();
    });
    httpServer->on("/api/v1/program", HTTP_GET, []() {
        defaultController->selectProgram();
    });

    httpServer->begin();
}

void loop() {
    httpServer->handleClient();
    MDNS.update();

    // if (timer1.state() == RUNNING) {
    //     timer1.update();
    // }
}
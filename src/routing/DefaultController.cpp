#include "DefaultController.h"

DefaultController::DefaultController(ESP8266WebServer* httpServer, LedRgb* ledRgb)
{
    this->httpServer = httpServer;
    this->ledRgb = ledRgb;
}

DefaultController::~DefaultController()
{
}

void DefaultController::mainRoute()
{
    this->httpServer->send(200, "application/json", "{\"name\":\"Led Rgb Strip Driver\", \"description\":\"\", \"apiUrl\":\"/api/v1\"}");
}

void DefaultController::getColorRoute()
{
    DynamicJsonDocument response(1024);

    response["red"] = this->ledRgb->getColor().red;
    response["green"] = this->ledRgb->getColor().green;
    response["blue"] = this->ledRgb->getColor().blue;

    String serializedResponse;
    serializeJson(response, serializedResponse);

    this->httpServer->send(200, "application/json", serializedResponse);
}

void DefaultController::changeColorRoute()
{
    if (this->httpServer->hasArg("plain") == false) {
        this->httpServer->send(400, "application/json", "{\"status\":\"error\"}");
        return;
    }
 
    String body = this->httpServer->arg("plain");
 
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, body);

    if (error) {
        this->httpServer->send(400, "application/json", "{\"status\":\"error\"}");
        return;
    }
    
    String red = doc["red"];
    String green = doc["green"];
    String blue = doc["blue"];

    Color newColor;
    newColor.red = red.toInt() % 256;
    newColor.green = green.toInt() % 256;
    newColor.blue = blue.toInt() % 256;

    this->ledRgb->changeColor(newColor);

    DynamicJsonDocument response(1024);

    response["red"] = newColor.red;
    response["green"] = newColor.green;
    response["blue"] = newColor.blue;

    String serializedResponse;
    serializeJson(response, serializedResponse);

    this->httpServer->send(200, "application/json", serializedResponse);
}

void DefaultController::statusGetRoute()
{
    String status = this->ledRgb->isEnabled() ? "enabled" : "disabled";

    this->httpServer->send(200, "application/json", "{\"status\":\"" + status + "\"}");
}

void DefaultController::changeStatusRoute()
{
    if (this->httpServer->hasArg("plain") == false) {
        this->httpServer->send(400, "application/json", "{\"status\":\"error\"}");
        return;
    }
 
    String body = this->httpServer->arg("plain");
 
    DynamicJsonDocument doc(1024);
    DeserializationError error = deserializeJson(doc, body);

    if (error) {
        this->httpServer->send(400, "application/json", "{\"status\":\"error\"}");
        return;
    }
    
    String status = doc["status"];

    if (status == "enable") {
        this->ledRgb->enable();
    } else if (status == "disable") {
        this->ledRgb->disable();
    }

    this->statusGetRoute();
}

void DefaultController::selectProgram()
{
    if (this->httpServer->hasArg("id") == false) {
        DynamicJsonDocument response(1024);

        response["status"] = "error";
        response["message"] = "Argument id is not passed";

        String serializedResponse;
        serializeJson(response, serializedResponse);

        this->httpServer->send(400, "application/json", serializedResponse);
    
        return;
    }

    String programId = this->httpServer->arg("id");

    if (programId == "1") {

    }

    DynamicJsonDocument response(1024);

    response["status"] = "Not found";
    response["message"] = "Program doesnt not exist";

    String serializedResponse;
    serializeJson(response, serializedResponse);

    this->httpServer->send(404, "application/json", serializedResponse);

    return;
}
#ifndef __DEFAULT_CONTROLLER_H__
#define __DEFAULT_CONTROLLER_H__

#ifndef Arduino_h
    #include <Arduino.h>
#endif
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>
#include "../models/LedRgb.h"
#include "consts.h"

class DefaultController
{
    ESP8266WebServer* httpServer;
    LedRgb* ledRgb;

public:
	DefaultController(ESP8266WebServer* httpServer, LedRgb* ledRgb);
	~DefaultController();
    void mainRoute();
    void getColorRoute();
    void changeColorRoute();
    void statusGetRoute();
    void changeStatusRoute();
    void selectProgram();
};

#endif
#ifndef __LED_RGB_H__
#define __LED_RGB_H__

#include <Arduino.h>
#include <vector>

enum Program
{
    MANUAL, AUTO
};

struct Color 
{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

inline bool operator==(const Color& a, const Color& b)
{
    return a.red == b.red && a.green == b.green && a.blue == b.blue;
}

const Color RED = { 255, 0, 0 };
const Color GREEN = { 0, 255, 0 };
const Color BLUE = { 0, 0, 255 };
const Color ORANGE = { 255, 165, 0 };
const Color YELLOW = { 255, 255, 0 };
const Color PURPLE = { 148, 0, 211 };
const Color INDIGO = { 75, 0, 130 };
const Color VIOLET = { 238, 130, 208 };
const Color WHITE = { 255, 255, 255 };

const std::vector<Color> RAINBOW = {RED, ORANGE, YELLOW, GREEN, BLUE, VIOLET, PURPLE};

class LedRgb
{
	Color color;
    Program program;
    bool status;
    uint8_t redColorPin;
    uint8_t greenColorPin;
    uint8_t blueColorPin;

public:
	LedRgb(uint8_t greenColorPin, uint8_t redColorPin, uint8_t blueColorPin);
	~LedRgb();
    Color getColor();
    void changeColor(Color color);
    void enable();
    void disable();
    bool isEnabled();
};

#endif //__LED_RGB_H__

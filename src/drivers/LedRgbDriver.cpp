#include "LedRgbDriver.h"

LedRgbDriver::LedRgbDriver(LedRgb *ledRgb)
{
    this->ledRgb = ledRgb;
    this->speed = 100;
    this->brightness = 100;
    this->transition = FADE;
    this->colorList = RAINBOW;
}

LedRgbDriver::~LedRgbDriver()
{
}

void LedRgbDriver::start()
{
    Serial.println("Start");

    // jezeli jest wybrane przejscie FADE to zmieniaj z koloru na kolor powoli (delay)
    // FLAST, odrazu zmiana na inny kolor, delay wiekszy niz up
    // FADE TO ZERO, to samo co 1 tylko z koloru do zera i na nowy kolor

    this->colorStart = colorList.begin();
    this->currentColor = this->colorStart;
    this->colorEnd = std::next(this->currentColor);
}

void LedRgbDriver::update()
{
    Serial.println("Update");

    if (*this->currentColor == *this->colorEnd) {
        // kolor zostal osiagniety zmiana na nastepny
        // jak sprawdzac kiedy koniec?

        this->colorStart = this->currentColor;

        if (this->colorEnd == this->colorList.end()) {
            this->colorEnd = this->colorList.begin();
        } else {
            this->colorEnd = std::next(this->currentColor);
        }
    }

    *this->currentColor = this->increase(*this->colorStart, *(std::next(this->currentColor)), *this->currentColor);
    

// z jakiego coloru (current)
// do jakiego dązy

    // jezeli jest wybrane przejscie FADE to zmieniaj z koloru na kolor powoli (delay)
    // FLAST, odrazu zmiana na inny kolor, delay wiekszy niz up
    // FADE TO ZERO, to samo co 1 tylko z koloru do zera i na nowy kolor
}

Color increase(Color from, Color finish, Color now)
{
    int inc = 0;
    float STEPS = 20.0;

    if (finish.red < from.red) {
        inc = floor((finish.red - from.red) / STEPS);
    } else {
        inc = ceil((finish.red - from.red) / STEPS);
    }
    
    if (now.red <= finish.red && now.red + inc >= finish.red) {
        now.red = finish.red; 
    } else if (now.red >= finish.red && now.red + inc <= finish.red) {
        now.red = finish.red;
    } else {
        now.red += inc;
    }

    if (finish.green < from.green) {
        inc = floor((finish.green - from.green) / STEPS);
    } else {
        inc = ceil((finish.green - from.green) / STEPS);
    }
    
    if (now.green <= finish.green && now.green + inc >= finish.green) {
        now.green = finish.green; 
    } else if (now.green >= finish.green && now.green + inc <= finish.green) {
        now.green = finish.green;
    } else {
        now.green += inc;
    }
    
    if (finish.blue < from.blue) {
        inc = floor((finish.blue - from.blue) / STEPS);
    } else {
        inc = ceil((finish.blue - from.blue) / STEPS);
    }
    
    if (now.blue <= finish.blue && now.blue + inc >= finish.blue) {
        now.blue = finish.blue; 
    } else if (now.blue >= finish.blue && now.blue + inc <= finish.blue) {
        now.blue = finish.blue;
    } else {
        now.blue += inc;
    }

    return now;
}

void LedRgbDriver::fade(std::vector<Color> colorList, int speed, LedRgb* ledRgb)
{
    for (auto color = colorList.begin(); color != colorList.end(); ++color) {
        Serial.print(1);       
    }
}
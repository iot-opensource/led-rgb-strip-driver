#ifndef __LED_RGB_DRIVER_H__
#define __LED_RGB_DRIVER_H__

#include <Arduino.h>
#include <vector>
#include "../models/LedRgb.h"

enum Transition
{
    FADE,
    FLASH,
    FATE_TO_ZERO
};

class LedRgbDriver {
    LedRgb* ledRgb;
    std::vector<Color> colorList;
    Transition transition;
    int speed;
    int brightness;

    std::vector<Color>::iterator currentColor;
    std::vector<Color>::iterator colorStart;
    std::vector<Color>::iterator colorEnd;
    int steps;

    void fade(std::vector<Color> colorList, int speed, LedRgb* ledRgb);

public:
	LedRgbDriver(LedRgb* ledRgb);
	~LedRgbDriver();
    void start();
    Color increase(Color from, Color finish, Color now);
    void update();
    // void stop();
    // void setColors(std::vector<Color> colorList);
    // void setTransition(Transition transition);
    // void setSpeed();
    // void setBrightness();
};

#endif //__LED_RGB_DRIVER_H__